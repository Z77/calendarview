package com.calendar.com;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Payal on 01-Sep-16.
 */
public class DataAdapter extends BaseAdapter {
    private Context context;
    private List<DataProvider> dataProviderList;

    public DataAdapter(Context context, List<DataProvider> dataProviderList) {
        this.context = context;
        this.dataProviderList = dataProviderList;
    }
    private class viewHolder{
        TextView date,event_title,event_desc;
    }

    @Override
    public int getCount() {
        return dataProviderList.size();
    }

    @Override
    public Object getItem(int position) {
        return dataProviderList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        viewHolder viewholder;
        if(convertView==null){
            LayoutInflater infla=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView=infla.inflate(R.layout.event_list_design,parent,false);
            viewholder=new viewHolder();
            viewholder.date=(TextView)convertView.findViewById(R.id.date_txt);
            viewholder.event_title=(TextView)convertView.findViewById(R.id.event_title);
            viewholder.event_desc=(TextView)convertView.findViewById(R.id.event_desc);
            convertView.setTag(viewholder);
        }
        else{
            viewholder=(viewHolder)convertView.getTag();
        }
        DataProvider data=(DataProvider)getItem(position);
        viewholder.date.setText(data.getDate());
        viewholder.event_title.setText(data.getEvent_title());
        viewholder.event_desc.setText(data.getEvent_desc());
        return convertView;
    }
}
