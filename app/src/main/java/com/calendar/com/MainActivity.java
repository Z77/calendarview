package com.calendar.com;

import android.graphics.Color;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Layout;
import android.text.style.AlignmentSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.widget.CalendarView;
import android.widget.TextView;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

public class MainActivity extends AppCompatActivity {



    MaterialCalendarView mcv;
    List<CalendarDay> list;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ViewPager viewPager=(ViewPager)findViewById(R.id.viewpager);
        setUpViewPager(viewPager);

        TabLayout tabs=(TabLayout)findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewPager);


    }
    private void setUpViewPager(ViewPager viewPager)
    {
        Adapter adapter=new Adapter(getSupportFragmentManager());
        adapter.addFragment(new DayFragment(),"Day");
        adapter.addFragment(new WeekFragment(),"Week");
        adapter.addFragment(new MonthFragment(),"Month");
        viewPager.setAdapter(adapter);
    }

    private class Adapter extends FragmentPagerAdapter
    {

        private final List<Fragment>mfragmentList=new ArrayList<>();
        private final List<String>mfragmentTitleList=new ArrayList<>();
        public Adapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return mfragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mfragmentList.size();
        }
        public void addFragment(Fragment fragment,String title)
        {
            mfragmentList.add(fragment);
            mfragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            //return super.getPageTitle(position);
            return mfragmentTitleList.get(position);
        }
    }
}
