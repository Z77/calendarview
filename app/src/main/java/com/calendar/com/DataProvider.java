package com.calendar.com;

/**
 * Created by Payal on 01-Sep-16.
 */
public class DataProvider {
    String date;
    String event_title;
    String event_desc;

    public DataProvider(String date, String event_title, String event_desc) {
        this.date = date;
        this.event_title = event_title;
        this.event_desc = event_desc;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getEvent_title() {
        return event_title;
    }

    public void setEvent_title(String event_title) {
        this.event_title = event_title;
    }

    public String getEvent_desc() {
        return event_desc;
    }

    public void setEvent_desc(String event_desc) {
        this.event_desc = event_desc;
    }
}
