package com.calendar.com;


import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.style.ImageSpan;
import android.text.style.SuperscriptSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.DayViewDecorator;
import com.prolificinteractive.materialcalendarview.DayViewFacade;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;
import com.prolificinteractive.materialcalendarview.OnDateSelectedListener;
import com.prolificinteractive.materialcalendarview.OnMonthChangedListener;
import com.prolificinteractive.materialcalendarview.spans.DotSpan;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonthFragment extends Fragment {

    MaterialCalendarView mcv;
    public static List<CalendarDay> list;
    ListView event_listview;

    public List<String> event_title;
    public List<String> event_desc;
    public List<String> event_date;
    List<DataProvider> dataProviderList;
    SharedPreferences prefs;
    public SharedPreferences.Editor editor;
    public MonthFragment() {
        // Required empty public constructor
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v=inflater.inflate(R.layout.fragment_month, container, false);
        mcv=(MaterialCalendarView)v.findViewById(R.id.calendar2);
        event_listview=(ListView)v.findViewById(R.id.event_list1);
        prefs=getContext().getSharedPreferences("DateList", Context.MODE_PRIVATE);

        list=new ArrayList<>();
        event_title=new ArrayList<>();
        event_desc=new ArrayList<>();
        event_date=new ArrayList<>();
        dataProviderList=new ArrayList<>();

        int no=1;
        for(int i=1;i<=10;i+=2){
            list.add(new CalendarDay(new Date(2016 - 1900, 9 - 1, i)));//year=currentyr-1900,month=currentmonth-1,date as it is
            String[] parcedt=String.valueOf(new CalendarDay(new Date(2016-1900,9-1,i))).split("-");
            String finaldt= (parcedt[2].replace("}",""));
            if(finaldt.length()>=1) {
                Log.i("newD", String.valueOf(finaldt.length()));
                  finaldt=0+finaldt;
            }
            event_date.add(finaldt);
            Log.i("FINALDATE", String.valueOf(finaldt));
            //event_date.add
            event_title.add("Eventno"+ no);
            event_desc.add("Eventdesc"+ no);
            no++;
        }
        for(int i=0;i<event_date.size();i++){
            dataProviderList.add(new DataProvider(event_date.get(i),event_title.get(i),event_desc.get(i)));
        }
        DataAdapter adapter=new DataAdapter(getContext(),dataProviderList);
        event_listview.setAdapter(adapter);



        //list.add(new CalendarDay(new Date(2016-1900,9-1,29)));//year=currentyr-1900,month=currentmonth-1,date as it is
        Log.i("LIST", String.valueOf(new CalendarDay(new Date(2016 - 1900, 8,29))));
        mcv.addDecorator(new EventDecorator(Color.RED, list));

        //mcv.setCurrentDate(CalendarDay.today());
        // mcv.setDateSelected(CalendarDay.today(),true);
        //mcv.setSelected(true);

        mcv.setSelectedDate(CalendarDay.today());
        mcv.setSelected(true);
        Log.i("Size", String.valueOf(list.size()));
        /*if(list.size()>0)
            Log.i("Size2", String.valueOf(list.size()));
            mcv.addDecorator(new EventDecorator(Color.RED,list));*/

        mcv.setOnMonthChangedListener(new OnMonthChangedListener() {
            @Override
            public void onMonthChanged(MaterialCalendarView widget, CalendarDay date) {
                Toast.makeText(getContext(),"Month"+(date.getMonth()+1)+"\nDate"+date,Toast.LENGTH_SHORT).show();
                String[] parcedate;
                parcedate=date.toString().split("-");
                Log.i("ParcedDate", String.valueOf(parcedate[1]));//gives current month
            }
        });
        mcv.setSelectionMode(MaterialCalendarView.SELECTION_MODE_MULTIPLE);
        //mcv.getSelectedDates()
        Log.i("selected", String.valueOf(mcv.getSelectedDates()));
        mcv.setOnDateChangedListener(new OnDateSelectedListener() {
            @Override
            public void onDateSelected(MaterialCalendarView widget, CalendarDay date, boolean selected) {
                int m=date.getMonth()+1;
                //list.add(date);
                Toast.makeText(getContext(),"SelectedDate:"+date.getDay()+"/"+m+"/"+date.getYear(),Toast.LENGTH_SHORT).show();
                Log.i("Date", String.valueOf(date));
                //editor= prefs.edit();

                //widget.addDecorator(new EventDecorator(Color.RED,list));
                //EventDecorator ED=new EventDecorator(123123,list);
                //ED.shouldDecorate(date);
                Log.i("payaldate","datechanged");
            }
        });
        return v;
    }
    public class EventDecorator implements DayViewDecorator {
        int color;
        List<CalendarDay> dates;

        public EventDecorator(int color, List<CalendarDay> dates) {
            this.color = color;
            this.dates = dates;
        }

        @Override
        public boolean shouldDecorate(CalendarDay day) {
            Log.i("Paaaa", String.valueOf(dates));
            String newd=String.valueOf(day).replace("[","");
            String finald=newd.replace("]", "");
            Log.i("Payalb", String.valueOf(finald));

            Log.i("payalbool", String.valueOf(dates.contains(finald)));
            return dates.contains(day);
            //return false;
        }

        @Override
        public void decorate(DayViewFacade view) {
            //view.addSpan(new SuperscriptSpan());
            //view.addSpan(new ImageSpan(getContext(),R.drawable.event_list_dt_back));
            view.addSpan(new DotSpan(5, Color.RED));
            Log.i("payal","deocrate"+view);
        }
    }
    public int pxToDp(int px){
        Resources resources = getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp =Math.round(px / ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }

}
